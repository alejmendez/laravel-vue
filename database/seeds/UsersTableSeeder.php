<?php

use Illuminate\Database\Seeder;

use App\User;


class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();
        User::flushEventListeners();

        User::create([
            'name' => 'Administrador',
            'email' => 'admin@gmail.com',
            'password' => '1234qwer',
            'verified' => true,
            //'verification_token' => '',
        ]);

        factory(User::class, 70)->create();
    }
}
